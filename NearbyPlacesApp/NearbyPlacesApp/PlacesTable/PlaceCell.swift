//
//  PlaceCell.swift
//  NearbyPlacesApp
//
//  Created by Dmitry on 17.03.2022.
//

import UIKit

final class PlaceCell: UICollectionViewCell {
    
    private(set) var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        
        return label
    }()
    
    private(set) var addressLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 5
        
        return label
    }()
    
    private(set) var coordinatesLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(nameLabel)
        addSubview(addressLabel)
        addSubview(coordinatesLabel)
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
            addressLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
            coordinatesLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
            //
            nameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            addressLabel.leadingAnchor.constraint(equalTo: self.nameLabel.trailingAnchor, constant: 20),
            coordinatesLabel.leadingAnchor.constraint(equalTo: self.addressLabel.trailingAnchor, constant: 20),
            coordinatesLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            //
            nameLabel.widthAnchor.constraint(equalToConstant: 100),
            addressLabel.widthAnchor.constraint(equalToConstant: 140),
            coordinatesLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 60)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
