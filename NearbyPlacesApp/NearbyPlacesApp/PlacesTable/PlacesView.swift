//
//  PlacesView.swift
//  NearbyPlacesApp
//
//  Created by Dmitry on 17.03.2022.
//

import NearbyPlaces
import UIKit

final class PlacesView: UIView {
    
    var places: [Place] {
        get {
            return tableView.places
        }
        set {
            tableView.places = newValue
        }
    }
    
    let startButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Start", for: .normal)
        button.backgroundColor = .gray
        button.isEnabled = false
        
        return button
    }()
    
    let stopButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Stop", for: .normal)
        button.backgroundColor = .gray
        button.isEnabled = false
        
        return button
    }()
    
    let errorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .red
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        label.text = "Location should be enabled"
        label.isHidden = true
        
        return label
    }()
    
    let statusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let tableView: PlacesTableView = {
        let view = PlacesTableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            startButton.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            stopButton.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            statusLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            //
            startButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            stopButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            //
            statusLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            //
            errorLabel.topAnchor.constraint(equalTo: startButton.bottomAnchor, constant: 20),
            errorLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            //
            tableView.topAnchor.constraint(equalTo: errorLabel.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    init() {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(startButton)
        self.addSubview(stopButton)
        self.addSubview(statusLabel)
        self.addSubview(errorLabel)
        self.addSubview(tableView)
        
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
