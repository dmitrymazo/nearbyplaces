//
//  PlacesTableView.swift
//  NearbyPlacesApp
//
//  Created by Dmitry on 16.03.2022.
//

import Foundation
import NearbyPlaces
import UIKit

final class PlacesTableView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    private static let cellId = "cellId"
    
    var places = [Place]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    private let collectionView: UICollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return places.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Self.cellId, for: indexPath) as? PlaceCell else { return UICollectionViewCell() }
        let place = places[indexPath.row]
        cell.nameLabel.text = place.name
        cell.addressLabel.text = place.address
        cell.coordinatesLabel.text = "\(place.coordinates.latitude.rounded(toPlaces: 6))\n\(place.coordinates.longitude.rounded(toPlaces: 6))"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 90)
    }
    
    init() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        super.init(frame: .zero)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(PlaceCell.self, forCellWithReuseIdentifier: Self.cellId)
        collectionView.backgroundColor = .lightGray
        self.addSubview(collectionView)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: self.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
