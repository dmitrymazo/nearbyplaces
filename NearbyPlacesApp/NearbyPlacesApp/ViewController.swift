//
//  ViewController.swift
//  NearbyPlacesApp
//
//  Created by Dmitry on 15.03.2022.
//

import Combine
import NearbyPlaces
import UIKit

final class ViewController: UIViewController {
    
    private struct Constants {
        static let idleStatusText = "Idle"
        static let receivingLocationUpdatesStatusText = "Receiving location updates"
    }
    
    // Select the necessary place loader:
//    private let placesLoader = FakeNearbyPlacesLoader()
//    private let placesLoader = GeoapifyNearbyPlacesLoader()
    private let placesLoader = HereApiNearbyPlacesLoader()
    
    private var cancellables = Set<AnyCancellable>()
    private let placesView = PlacesView()
    
    private func setupConstraints() {
        placesView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            placesView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            placesView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            placesView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            placesView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    private func setupPlacesLoader() {
        setupView()
        observeStatus()
        observePlaces()
        placesLoader.requestLocationAccessPermission()
    }
    
    private func observePlaces() {
        placesLoader.$places
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let err):
                        print(err)
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] places in
                    DispatchQueue.main.async {
                        self?.placesView.places = places
                    }
                }
            )
            .store(in: &cancellables)
    }
    
    private func observeStatus() {
        placesLoader.$authorizationStatus
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let err):
                        print(err)
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] status in
                    DispatchQueue.main.async {
                        self?.received(status: status)
                    }
                }
            )
            .store(in: &cancellables)
    }
    
    private func received(status: LocationAuthorizationStatus) {
        guard placesLoader.authorizationStatus != .notDetermined else { return }
        guard [.authorized,
               .authorizedAlways,
               .authorizedWhenInUse].contains(placesLoader.authorizationStatus) else {
            placesView.errorLabel.isHidden = false
            return
        }
        
        placesView.errorLabel.isHidden = true
        placesView.startButton.isEnabled = true
    }
    
    private func setupView() {
        placesView.startButton.addTarget(self, action: #selector(self.startButtonTapped), for: .touchUpInside)
        placesView.stopButton.addTarget(self, action: #selector(self.stopButtonTapped), for: .touchUpInside)
        placesView.statusLabel.text = Constants.idleStatusText
    }
    
    @objc
    private func startButtonTapped(sender: UIButton) {
        placesView.startButton.isEnabled = false
        placesView.stopButton.isEnabled = true
        placesView.statusLabel.text = Constants.receivingLocationUpdatesStatusText
        placesLoader.startReceivingPlacesNearLocation()
    }
    
    @objc
    private func stopButtonTapped(sender: UIButton) {
        placesView.startButton.isEnabled = true
        placesView.stopButton.isEnabled = false
        placesView.statusLabel.text = "Idle"
        placesLoader.stopRealTimeLocationUpdates()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(placesView)
        
        setupConstraints()
        setupPlacesLoader()
    }
    
}

