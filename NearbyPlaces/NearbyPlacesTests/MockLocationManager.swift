//
//  MockLocationManager.swift
//  NearbyPlacesTests
//
//  Created by Dmitry on 16.03.2022.
//

import Combine
@testable
import NearbyPlaces

final class MockLocationManager: LocationManager {
    
    private var coordinateSubject = PassthroughSubject<Coordinates, Never>()
    private var statusSubject = PassthroughSubject<LocationAuthorizationStatus, Never>()
    
    lazy var coordinatePublisher: AnyPublisher<Coordinates, Never> = coordinateSubject.eraseToAnyPublisher()
    
    lazy var statusPublisher: AnyPublisher<LocationAuthorizationStatus, Never> = statusSubject.eraseToAnyPublisher()
    
    func requestPermission() { }
    
    func getRealTimeLocationUpdates() { }
    
    func stopRealTimeLocationUpdates() { }
    
    func invoke(coordinates: Coordinates) {
        coordinateSubject.send(coordinates)
    }
    
}
