//
//  NearbyPlacesLoaderTests.swift
//  NearbyPlacesTests
//
//  Created by Dmitry on 16.03.2022.
//

@testable
import NearbyPlaces
import XCTest

final class NearbyPlacesLoaderTests: XCTestCase {
    
    func testLoader() {
        let places = [
            Place(name: "First fake place",
                  address: "Fake address 1",
                  coordinates: Coordinates(latitude: 1, longitude: 2)),
            Place(name: "Second fake place",
                  address: "Fake address 2",
                  coordinates: Coordinates(latitude: 3, longitude: 4)),
        ]
        let coordinates = Coordinates(latitude: 3, longitude: 4)
        let locationManager = MockLocationManager()
        let networkService = FakeNearbyPlacesNetworkService()
        let loader = NearbyPlacesLoader(locationManager: locationManager, networkService: networkService)
        
        XCTAssertEqual(loader.places.count, 0)
        
        locationManager.invoke(coordinates: coordinates)
        XCTAssertEqual(loader.places.count, 0)
        
        networkService.invoke(places: places)
        XCTAssertEqual(loader.places.count, 2)
    }
    
}
