//
//  Double+Extension.swift
//  NearbyPlaces
//
//  Created by Dmitry on 17.03.2022.
//

import Foundation

public extension Double {
    
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
}
