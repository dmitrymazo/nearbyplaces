//
//  NearbyPlacesLoader.swift
//  NearbyPlaces
//
//  Created by Dmitry on 16.03.2022.
//

import Combine
import Foundation

public class NearbyPlacesLoader {
    
    private let locationManager: LocationManager
    private let nearbyPlacesNetworkService: NearbyPlacesNetworkService
    private var cancellables = Set<AnyCancellable>()
    
    @Published
    public private(set) var places = [Place]()
    
    @Published
    public var authorizationStatus: LocationAuthorizationStatus = .notDetermined
    
    // MARK: - Private
    
    private func observeLocationChanges() {
        locationManager.coordinatePublisher
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let err):
                        print(err)
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] coordinates in
                    guard !coordinates.isZero else { return }
                    self?.received(coordinates: coordinates)
                }
            )
            .store(in: &cancellables)
    }
    
    private func observeStatusChanges() {
        locationManager.statusPublisher
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let err):
                        print(err)
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] status in
                    self?.authorizationStatus = status
                }
            )
            .store(in: &cancellables)
    }
    
    private func received(coordinates: Coordinates) {
        self.nearbyPlacesNetworkService.places(near: coordinates)
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let err):
                        print(err)
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] places in                    
                    self?.places = places
                }
            )
            .store(in: &self.cancellables)
    }
    
    // MARK: - Public
    
    public func requestLocationAccessPermission() {
        locationManager.requestPermission()
    }
    
    public func startReceivingPlacesNearLocation() {
        locationManager.getRealTimeLocationUpdates()
    }
    
    public func stopRealTimeLocationUpdates() {
        locationManager.stopRealTimeLocationUpdates()
    }
    
    // MARK: - Init
    
    init(locationManager: LocationManager, networkService: NearbyPlacesNetworkService) {
        self.locationManager = locationManager
        self.nearbyPlacesNetworkService = networkService
        self.observeLocationChanges()
        self.observeStatusChanges()
    }
    
}
