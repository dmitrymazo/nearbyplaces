//
//  HereApiNearbyPlacesLoader.swift
//  NearbyPlaces
//
//  Created by Dmitry on 16.03.2022.
//

import Foundation

public final class HereApiNearbyPlacesLoader: NearbyPlacesLoader {
    public init() {
        let locationManager = DefaultLocationManager()
        let networkService = HereApiNearbyPlacesNetworkService()
        
        super.init(locationManager: locationManager, networkService: networkService)
    }
}
