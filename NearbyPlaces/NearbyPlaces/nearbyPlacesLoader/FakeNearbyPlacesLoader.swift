//
//  FakeNearbyPlacesLoader.swift
//  NearbyPlaces
//
//  Created by Dmitry on 16.03.2022.
//

import Combine
import Foundation

public final class FakeNearbyPlacesLoader: NearbyPlacesLoader {
    
    private let networkService = FakeNearbyPlacesNetworkService()
    private var cancellables = Set<AnyCancellable>()
    
    private func setPlacesOnLocationChange(locationManager: LocationManager) {
        locationManager.coordinatePublisher
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let err):
                        print(err)
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] _ in
                    guard let self = self else { return }
                    self.networkService.invoke(places: FakePlaces.generate())
                }
            )
            .store(in: &cancellables)
    }
    
    public init() {
        let locationManager = DefaultLocationManager()
        super.init(locationManager: locationManager, networkService: networkService)
        
        setPlacesOnLocationChange(locationManager: locationManager)
    }
    
}
