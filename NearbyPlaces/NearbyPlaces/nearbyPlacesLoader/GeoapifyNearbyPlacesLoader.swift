//
//  GeoapifyNearbyPlacesLoader.swift
//  NearbyPlaces
//
//  Created by Dmitry on 16.03.2022.
//

import Foundation

public final class GeoapifyNearbyPlacesLoader: NearbyPlacesLoader {
    public init() {
        let locationManager = DefaultLocationManager()
        let networkService = GeoapifyNearbyPlacesNetworkService()
        
        super.init(locationManager: locationManager, networkService: networkService)
    }
}
