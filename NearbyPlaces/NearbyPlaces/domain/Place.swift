//
//  Place.swift
//  NearbyPlaces
//
//  Created by Dmitry on 16.03.2022.
//

import Combine
import Foundation

public struct Coordinates {
    public let latitude: Double
    public let longitude: Double
    
    public var isZero: Bool { return latitude == 0 && longitude == 0 }
}

public struct Place {
    public let name: String
    public let address: String
    public let coordinates: Coordinates
}
