//
//  FakePlaces.swift
//  NearbyPlaces
//
//  Created by Dmitry on 17.03.2022.
//

import Foundation

private final class RandomCoordinateGenerator {
    
    static func generate() -> Coordinates {
        Coordinates(latitude: Double.random(in: 30..<40), longitude: Double.random(in: 30..<40))
    }
    
}

final class FakePlaces {
    
    static func generate() -> [Place] {
        return [
            Place(name: "First fake place",
                  address: "Some Long Named Road 1, Some Important County, Some Country",
                  coordinates: RandomCoordinateGenerator.generate()),
            Place(name: "Second fake place",
                  address: "Via delle Quattro Fontane, 13, 00184 Roma RM, Italy",
                  coordinates: RandomCoordinateGenerator.generate()),
            Place(name: "Third fake place",
                  address: "Fake address 3",
                  coordinates: RandomCoordinateGenerator.generate()),
            Place(name: "Fourth fake place",
                  address: "Fake address 4",
                  coordinates: RandomCoordinateGenerator.generate()),
            Place(name: "Fifth fake place",
                  address: "Fake address 5",
                  coordinates: RandomCoordinateGenerator.generate())
        ]
    }
    
}
