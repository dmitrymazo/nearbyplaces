//
//  FakeNearbyPlacesNetworkService.swift
//  NearbyPlaces
//
//  Created by Dmitry on 16.03.2022.
//

import Combine
import Foundation

public final class FakeNearbyPlacesNetworkService: NearbyPlacesNetworkService {
    private lazy var publisher: AnyPublisher<[Place], Error> = subject.eraseToAnyPublisher()
    private var subject = PassthroughSubject<[Place], Error>()
    
    func places(near coordinates: Coordinates) -> AnyPublisher<[Place], Error> {
        return publisher
    }
    
    func invoke(places: [Place]) {
        subject.send(places)
    }
}
