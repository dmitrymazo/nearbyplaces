//
//  HereApiNearbyPlacesNetworkService.swift
//  NearbyPlaces
//
//  Created by Dmitry on 17.03.2022.
//

import Combine
import Foundation

final class HereApiNearbyPlacesNetworkService: NearbyPlacesNetworkService {
    
    private static let endPoint = "https://discover.search.hereapi.com/v1"
    private static let apiKey = "zEc7X-xrXM_bKbhhpULJx_Euq2T9Ah1jrXjgbsa66uw"
    private static let limit = 2
    
    private let networkService = DefaultNetworkService<PlaceResponseDto>()
    
    private static func url(forCoordinates coordinates: Coordinates) -> String {
        return "\(self.endPoint)/discover?at=\(coordinates.latitude),\(coordinates.longitude)&limit=\(Self.limit)&lang=en&q=restaurant&apiKey=\(Self.apiKey)"
    }
    
    func places(near coordinates: Coordinates) -> AnyPublisher<[Place], Error> {
        let urlString = Self.url(forCoordinates: coordinates)
        return networkService.data(fromUrl: urlString)
            .map { dtos in
                dtos?.items.map { PlacesMapper.place(from: $0) }
                ?? []
            }.eraseToAnyPublisher()
    }
    
}

private struct PlaceResponseDto: Decodable {
    let items: [PlaceDto]
}

private struct PlaceDto: Decodable {
    let title: String
    let address: PlaceAddressDto
    let position: PlacePositionDto
}

private struct PlacePositionDto: Decodable {
    let lat: Double
    let lng: Double
}

private struct PlaceAddressDto: Decodable {
    let label: String
}

private final class PlacesMapper {
    
    static func place(from dto: PlaceDto) -> Place {
        let coordinates = Coordinates(latitude: dto.position.lat, longitude: dto.position.lng)
        return Place(name: dto.title,
                     address: dto.address.label,
                     coordinates: coordinates)
    }
    
}
