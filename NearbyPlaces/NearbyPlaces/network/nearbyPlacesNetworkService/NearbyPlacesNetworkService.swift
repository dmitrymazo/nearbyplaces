//
//  NearbyPlacesNetworkService.swift
//  NearbyPlaces
//
//  Created by Dmitry on 16.03.2022.
//

import Combine
import Foundation

protocol NearbyPlacesNetworkService {
    func places(near coordinates: Coordinates) -> AnyPublisher<[Place], Error>
}
