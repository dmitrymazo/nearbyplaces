//
//  GeoapifyNearbyPlacesNetworkService.swift
//  NearbyPlaces
//
//  Created by Dmitry on 17.03.2022.
//

import Combine
import Foundation

final class GeoapifyNearbyPlacesNetworkService: NearbyPlacesNetworkService {
    
    private static let endPoint = "https://api.geoapify.com/v2"
    private static let apiKey = "236d1f10a4ce42c2a5a973b4b6727e45"
    private static let limit = 5
    
    private let networkService = DefaultNetworkService<PlaceResponseDto>()
    
    private static func url(forCoordinates coordinates: Coordinates) -> String {
        return "\(self.endPoint)/places?categories=catering.restaurant&bias=proximity:\(coordinates.latitude),\(coordinates.longitude)&limit=\(Self.limit)&apiKey=\(Self.apiKey)"
    }
    
    func places(near coordinates: Coordinates) -> AnyPublisher<[Place], Error> {
        let urlString = Self.url(forCoordinates: coordinates)
        return networkService.data(fromUrl: urlString)
            .map { dtos in
                dtos?.features.map { PlacesMapper.place(from: $0.properties) }
                ?? []
            }.eraseToAnyPublisher()
    }
    
}

private struct PlaceResponseDto: Decodable {
    let features: [FeatureDto]
}

private struct FeatureDto: Decodable {
    let properties: PlaceDto
}

private struct PlaceDto: Decodable {
    let name: String
    let address_line2: String
    let lat: Double
    let lon: Double
}

private final class PlacesMapper {
    
    static func place(from dto: PlaceDto) -> Place {
        let coordinates = Coordinates(latitude: dto.lat, longitude: dto.lon)
        return Place(name: dto.name,
                     address: dto.address_line2,
                     coordinates: coordinates)
    }
    
}
