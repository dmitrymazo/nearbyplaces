//
//  NetworkService.swift
//  NearbyPlaces
//
//  Created by Dmitry on 16.03.2022.
//

import Combine
import Foundation

protocol NetworkService {
    func data(fromUrl url: String) -> AnyPublisher<Data?, Error>
}

open class DefaultNetworkService<Entity> where Entity: Decodable {
    
    private var backgroundQueue = DispatchQueue(label: "network.service.queue")
    
    func data(fromUrl url: String) -> AnyPublisher<Entity?, Error> {
        let urlSession = URLSession.shared
        guard let url = URL(string: url) else {
            return Fail(error: NetworkError.defaultError).eraseToAnyPublisher()
        }
        
        var request = URLRequest(url: url)
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        return urlSession.dataTaskPublisher(for: request)
            .subscribe(on: backgroundQueue)
            .tryMap { data, response -> Entity? in
                guard let resp = response as? HTTPURLResponse else {
                    return nil
                }
                
                if 200 ..< 300 ~= resp.statusCode {
                    let response = try? JSONDecoder().decode(Entity.self, from: data)
                    return response
                } else {
                    throw NetworkError.defaultError
                }
            }
            .mapError { error -> NetworkError in
                (error as? NetworkError) ?? .defaultError
            }
            .eraseToAnyPublisher()
    }
    
}

public enum NetworkError: Error {
    case defaultError
}
