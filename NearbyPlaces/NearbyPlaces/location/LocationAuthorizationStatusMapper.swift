//
//  LocationAuthorizationStatusMapper.swift
//  NearbyPlaces
//
//  Created by Dmitry on 17.03.2022.
//

import CoreLocation

public enum LocationAuthorizationStatus {
    case notDetermined
    case restricted
    case denied
    case authorizedAlways
    case authorizedWhenInUse
    case authorized
    case unknown
}

final class LocationAuthorizationStatusMapper {
    
    static func status(from locationStatus: CLAuthorizationStatus) -> LocationAuthorizationStatus {
        switch locationStatus {
        case .notDetermined:
            return .notDetermined
        case .restricted:
            return .restricted
        case .denied:
            return .denied
        case .authorizedAlways:
            return .authorizedAlways
        case .authorizedWhenInUse:
            return .authorizedWhenInUse
        case .authorized:
            return .authorized
        @unknown default:
            return .unknown
        }
    }
    
}
