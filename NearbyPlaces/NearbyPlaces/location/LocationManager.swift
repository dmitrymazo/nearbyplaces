//
//  LocationManager.swift
//  NearbyPlaces
//
//  Created by Dmitry on 15.03.2022.
//

import Combine
import CoreLocation
import Foundation
import UIKit

public protocol LocationManager {
    var coordinatePublisher: AnyPublisher<Coordinates, Never> { get }
    var statusPublisher: AnyPublisher<LocationAuthorizationStatus, Never> { get }
    
    func requestPermission()
    func getRealTimeLocationUpdates()
    func stopRealTimeLocationUpdates()
}

public final class DefaultLocationManager: NSObject, CLLocationManagerDelegate, LocationManager {
    
    lazy public var coordinatePublisher: AnyPublisher<Coordinates, Never> = coordinateSubject.eraseToAnyPublisher()
    private var coordinateSubject = PassthroughSubject<Coordinates, Never>()
    
    lazy public var statusPublisher: AnyPublisher<LocationAuthorizationStatus, Never> = statusSubject.eraseToAnyPublisher()
    private var statusSubject = PassthroughSubject<LocationAuthorizationStatus, Never>()
    
    private let locationManager = CLLocationManager()
    
    public var authorizationStatus: LocationAuthorizationStatus {
        return LocationAuthorizationStatusMapper.status(from: locationManager.authorizationStatus)
    }
    
    public func requestPermission() {
        locationManager.requestAlwaysAuthorization()
    }
    
    public func getRealTimeLocationUpdates() {
        locationManager.startUpdatingLocation()
    }
    
    public func stopRealTimeLocationUpdates() {
        locationManager.stopUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        let authStatus = LocationAuthorizationStatusMapper.status(from: locationManager.authorizationStatus)
        statusSubject.send(authStatus)
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let coordinates = Coordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        coordinateSubject.send(coordinates)
    }
    
    public override init() {
        super.init()
        locationManager.delegate = self
    }
    
}
