# NearbyPlaces
A library that gets restaurants near your location and an app that displays them in a table.

## Different nearby place APIs
There are 3 APIs included in the SDK.
- `HereApiNearbyPlacesLoader` - provided by hereapi.com
- `GeoapifyNearbyPlacesLoader` - provided by geoapify.com
- `FakeNearbyPlacesLoader` - shows places from a built-in array with fake names and random coordinates (created for testing purposes since all APIs have number of request limitations)

## Example of SDK usage

```swift
import Combine
import NearbyPlaces

class MyClass {

    private let placesLoader = HereApiNearbyPlacesLoader()
    private var cancellables = Set<AnyCancellable>()

    private func observePlaces() {
        placesLoader.$places
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let err):
                        print(err)
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] places in
                    DispatchQueue.main.async {
                        print("Nearby places: \(places.map { $0.name })")
                    }
                }
            )
            .store(in: &cancellables)
    }
    
    private func observeStatus() {
        placesLoader.$authorizationStatus
            .sink(
                receiveCompletion: { completion in
                    switch completion {
                    case .failure(let err):
                        print(err)
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] status in
                    DispatchQueue.main.async {
                        self?.received(status: status)
                    }
                }
            )
            .store(in: &cancellables)
    }

    private func received(status: LocationAuthorizationStatus) {
        guard placesLoader.authorizationStatus != .notDetermined else { return }
        guard [.authorized,
               .authorizedAlways,
               .authorizedWhenInUse].contains(placesLoader.authorizationStatus) else {
            // User disabled location access
            return
        }
        
        // User enabled location access
    }

    func main() {
        observeStatus()
        observePlaces()
        placesLoader.requestLocationAccessPermission()
    }

    deinit {
        placesLoader.stopRealTimeLocationUpdates()
    }

}
```

## Using the app
Click "Start" to start real-time location update and retrieving nearby places.
Click "Stop" to stop.